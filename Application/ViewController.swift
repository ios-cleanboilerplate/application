//
//  ViewController.swift
//  Application
//
//  Created by Behrad Kazemi on 6/6/22.
//

import UIKit
import CoreDI
import Domain
class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    let injectedPlatforms = CoreDI.injectPlatformInDomain()
    print(injectedPlatforms.getExampleUseCase().getModel().fieldName)
    // Do any additional setup after loading the view.
  }


}

